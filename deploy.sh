# clean up old app
pm2 kill

nvm install 16

npm remove pm2 -g
npm -v
node -v

# installing dependencies
echo "Installing pm2"
npm install pm2 -g
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run start:production