import mongoose from "mongoose";
import logger from "../logger"

async function connectToDb(mongoURL:string):Promise<void> {
  try {
    await mongoose.connect(mongoURL);
  } catch (error) {
    // TODO: replace with the logger.error()
    logger.error(error)
  }

}

export default connectToDb;