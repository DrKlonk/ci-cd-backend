import express, { Application, Request, Response } from "express"
import path from "path"
import { v4 as uuidv4, validate } from "uuid"
import cors from "cors"
import basicAuth from "express-basic-auth"
import swaggerUi from "swagger-ui-express"
import YAML from "yamljs"

import MovieModel from "./models/MovieModel"
import connectToDb from "./models/connectToDb"
import { loggerMiddleware } from "./logger"

const swaggerDocument = YAML.load(path.join(__dirname, "./swagger.yaml"))

interface IUserList {
  [key: string]: string
}

export interface IServerConfig {
  databaseUrl: string
  users: IUserList
}

async function createServer(config: IServerConfig): Promise<Application> {
  const app = express()

  await connectToDb(config.databaseUrl)

  // Middleware
  app.use(loggerMiddleware)
  app.use(
    cors({
      origin: "https://myfrontend.com",
    })
  )
  app.use(express.json()) // Content-Type --> parse instead of JSON.parse()
  app.use(
    "/api",
    basicAuth({
      // applied only to the api endpoints
      users: config.users, // this should be a variable in .env
    })
  )
  console.log(config.users)
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument))
  app.get(
    "/api/movies",
    async (request: Request, response: Response): Promise<void> => {
      const movieList = await MovieModel.find({}).lean()
      response.json(movieList)
    }
  )

  app.get(
    "/api/movies/:id",
    async (request: Request, response: Response): Promise<void> => {
      try {
        const id: string = request.params.id
        if (validate(id)) {
          const movie = await MovieModel.findOne({ id }).lean()
          if (movie) {
            response.json(movie)
          } else {
            response
              .status(404)
              .json({ message: `Movie with id:${id} was not found` })
          }
        } else {
          response.status(400).json({ message: `Id:${id} is not a valid uuid` })
        }
      } catch (error) {
        response.status(500).json({ message: `An internal error has occurred` })
      }
    }
  )

  app.delete(
    "/api/movies/:id",
    async (request: Request, response: Response): Promise<void> => {
      const id: string = request.params.id
      const movie = await MovieModel.findOneAndDelete({ id }).lean()
      response.json(movie)
    }
  )

  app.post(
    "/api/movies",
    async (request: Request, response: Response): Promise<void> => {
      const movieData = request.body

      const newMovie = await MovieModel.create({
        ...movieData,
        id: uuidv4(), // new id for each movie
      })
      response.json(newMovie)
    }
  )

  return app
}

export default createServer
