import dotenv from "dotenv"
import { Application } from "express"

import createServer, { IServerConfig } from "./createServer"

// loading env variables
dotenv.config()

const config: IServerConfig = {
  databaseUrl: process.env.DB_URL || "",
  users: {
    admin: process.env.AUTH_PASS || "",
  },
}
createServer(config).then((app: Application) => {
  app.listen(3000, () => {
    console.info("Server is running on: http://localhost:3000/")
  })
})
